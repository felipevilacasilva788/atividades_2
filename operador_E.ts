//testando o operador E - &&
namespace operador_E
{
    let idade = 20;
    let maiorIdade = idade > 18;
    let possuiCarteiraDeMotorista = true;
    let podeDirigir = maiorIdade && possuiCarteiraDeMotorista;
    console.log(podeDirigir); // true 
}